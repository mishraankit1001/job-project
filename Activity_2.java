package testcases;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_2 {

	WebDriver driver = new FirefoxDriver();
	
	    @BeforeMethod
        public void beforeMethod() 
	     {
              //Open browser
        driver.get("https://alchemy.hguy.co/jobs/");
         }
	
    @Test
    public void Header1() 
        {
    	WebElement Heading1 = driver.findElement(By.cssSelector(".entry-title"));
        Assert.assertTrue(Heading1.isDisplayed());
        System.out.println("Heading on the page is: " + Heading1.getText());
          //Assertion for page heading
        Assert.assertEquals(Heading1.getText(), "Welcome to Alchemy Jobs");
        }
    
    @AfterMethod
    public void afterMethod() 
    {
        //Close the browser
        driver.quit();
        
    }
}
