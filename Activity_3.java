package testcases;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_3 {

	WebDriver driver = new FirefoxDriver();
	
	    @BeforeMethod
        public void beforeMethod() 
	     {
              //Open browser
        driver.get("https://alchemy.hguy.co/jobs/");
         }
	
    @Test
    public void HeaderImage() 
        {
    	WebElement HeaderImage = driver.findElement(By.xpath("//img[contains(@class,'large')]"));
              
        //Print for page heading
        System.out.println("Page Image URL is: " + HeaderImage.getAttribute("src"));
        }
    
    @AfterMethod
    public void afterMethod() 
    {
        //Close the browser
        driver.quit();
        
    }
}
