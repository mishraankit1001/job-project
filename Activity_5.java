package testcases;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_5 {

	WebDriver driver = new FirefoxDriver();
	
	    @BeforeMethod
        public void beforeMethod() 
	     {
              //Open browser
        driver.get("https://alchemy.hguy.co/jobs/");
         }
	
    @Test
    public void Menu1() 
        {
    	WebElement Menu1= driver.findElement(By.cssSelector("#menu-item-24 > a:nth-child(1)"));
              
        //Print for page heading
        System.out.println("Menu name on the header is: " + Menu1.getText());
      
        //Assertion for menu
        Assert.assertEquals(Menu1.getText(), "Jobs");
        
        //Click the menu
        Menu1.click();
       
       //Print title of new page
        System.out.println("Heading is: " + driver.getTitle());
        
       //Assertion for new page
        Assert.assertEquals(driver.getTitle(), "Jobs � Alchemy Jobs");
        }
    
    @AfterMethod
    public void afterMethod() 
    {
        //Close the browser
        driver.quit();
        
    }
}
